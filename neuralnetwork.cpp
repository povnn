#include <QDebug>
#include "neuralnetwork.h"

NeuralNetwork::NeuralNetwork()
{
}

NeuralNetwork::~NeuralNetwork()
{
}

QVector<Neuron*> &NeuralNetwork::getNeurons()
{
    return this->neurons;
}

void NeuralNetwork::addNeuron(Neuron *neuron)
{
   this->neurons.push_front(neuron);
}

void NeuralNetwork::calculate()
{
    /* TODO: more types */
    int size = this->neurons.size();

    for (int i = 0; i < size; i++) {
        this->neurons[i]->calculate();
        this->neurons[i]->endCalculate();
    }
}
