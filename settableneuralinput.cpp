#include "settableneuralinput.h"

SettableNeuralInput::SettableNeuralInput(double newValue ,NeuralInput *parent)
        : NeuralInput(parent), value(newValue)
{
}

double SettableNeuralInput::getValue()
{
    return this->value;
}
