# -------------------------------------------------
# Project created by QtCreator 2009-06-25T13:31:47
# -------------------------------------------------
QT += network \
    xml
QT -= gui
TARGET = NeuralNetworkTest
CONFIG += console
CONFIG -= app_bundle
TEMPLATE = app
SOURCES += main.cpp \
    neuralnetwork.cpp \
    neuron.cpp \
    neuralinput.cpp \
    settableneuralinput.cpp
HEADERS += neuralnetwork.h \
    neuron.h \
    neuralinput.h \
    settableneuralinput.h
