#ifndef NEURALNETWORK_H
#define NEURALNETWORK_H

#define UNUSED(var) if (0 && var) {}

#include <QObject>
#include <QVector>
#include "neuron.h"

class NeuralNetwork : public QObject
{
    Q_OBJECT

private:
    QVector<Neuron*> neurons;

public:
    NeuralNetwork();
    ~NeuralNetwork();
    QVector<Neuron*> &getNeurons();
    void addNeuron(Neuron *neuron);
    void removeNeuron(Neuron *neuron);
    void calculate();
};

#endif // NEURALNETWORK_H
