#include <QtCore/QCoreApplication>
#include <QDebug>
#include "neuralnetwork.h"
#include "settableneuralinput.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    NeuralNetwork nn;

    Neuron *n1 = new Neuron(Neuron::typeStandart);
    Neuron *n2 = new Neuron(Neuron::typeStandart);
    SettableNeuralInput *sni = new SettableNeuralInput(5);

    nn.addNeuron(n1);
    nn.addNeuron(n2);

    n2->addInput(sni, 1.0);
    n1->addInput(n2, 1.0);

    for (int i = 0; i < 10; i++) {
        nn.calculate();
    }

    qDebug() << "Neuron n1 : " << n1->getValue() << endl;

    a.exit();
}
