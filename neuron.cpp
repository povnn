#include "neuralnetwork.h"

Neuron::Neuron(unsigned int type, double threshold)
        : type(type), threshold(threshold)
{
}

Neuron::~Neuron()
{
}

void Neuron::addInput(NeuralInput *input, double weight)
{
    this->inputs.append(input);
    this->weights[input] = weight;
}

void Neuron::removeInput(NeuralInput *input)
{
    this->inputs.remove(this->inputs.indexOf(input));
    this->weights.remove(input);
}

double Neuron::getValue()
{
    return this->value;
}

void Neuron::calculate()
{
    switch (this->type) {
        case Neuron::typeStandart:
            this->newValue = 0;
            NeuralInput *input;
            int size = this->inputs.size();
            for (int i = 0; i < size; i++) {
                input = this->inputs[i];
                this->newValue += input->getValue() * this->weights[input];
            }
            this->newValue -= threshold;
            break;
    }
}

void Neuron::endCalculate()
{
    this->value = this->newValue;
}

void Neuron::setCorrectValue(double value)
{
    correctSum += value;
    correctCount ++;
}

void Neuron::learn()
{
    switch (this->learningType) {
        case Neuron::learningTypeStandart:
            switch (this->type) {
                case Neuron::typeStandart:
                    this->error = this->getValue() - (correctSum / correctCount);
                    correctSum = 0;
                    correctCount = 0;

                    double newWeight;
                    int size = this->inputs.size();
                    for (int i = 0; i < size; i++) {
                        newWeight = 0; // zatim nefunguje
                    }
                    break;
            }
            break;
    }
}

void Neuron::endLearn()
{

}


