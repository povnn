#ifndef SETTABLENEURALINPUT_H
#define SETTABLENEURALINPUT_H

#include <QObject>
#include "neuralnetwork.h"
#include "neuralinput.h"

class SettableNeuralInput : public NeuralInput
{
    Q_OBJECT

private:
    double value;

public:
    SettableNeuralInput(double newValue = 0, NeuralInput *parent = 0);
    double getValue();
    void setCorrectValue(double value) {UNUSED(value)};
    void setValue(double newValue);
};

#endif // SETTABLENEURALINPUT_H
