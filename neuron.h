#ifndef NEURON_H
#define NEURON_H

#include <QVector>
#include <QHash>
#include "neuralinput.h"

class Neuron : public NeuralInput
{
    Q_OBJECT

private:
    unsigned int type;
    unsigned int learningType;
    double threshold;
    double newValue;
    double value;
    double correctSum;
    unsigned int correctCount;
    double error;
    QVector<NeuralInput*> inputs;
    QHash<NeuralInput*, double> weights;

public:
    static const unsigned int typeStandart = 0;

    static const unsigned int learningTypeStandart = 0;

    Neuron(unsigned int type, double threshold = 0);
    ~Neuron();
    void addInput(NeuralInput *input, double weight);
    void removeInput(NeuralInput *input);
    void calculate();
    void endCalculate();
    void learn();
    void endLearn();
    double getValue();
    void setCorrectValue(double value);
};

#endif // NEURON_H
