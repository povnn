#ifndef NEURALINPUT_H
#define NEURALINPUT_H

#include <QObject>

class NeuralInput : public QObject
{
    Q_OBJECT

public:
    NeuralInput(QObject *parent = 0);
    virtual ~NeuralInput();
    virtual double getValue() = 0;
    virtual void setCorrectValue(double value) = 0;
};

#endif // NEURALINPUT_H
